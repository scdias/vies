VIES - VAT Information Exchange System
========================

Esta aplicação tem a intenção de facilitar a consulta de números fiscais no espaço europeu. 
A informação é baseada em dados obtidos da Comissão Europeia através do [serviço disponibilizado para o efeito](http://ec.europa.eu/taxation_customs/vies/vieshome.do).