/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vies;

import gui.FrmPrincipal;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

/**
 *
 * @author Sergio Dias (scdias@outlook.com)
 */
public class VIES {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException ex) {
            System.err.println("Erro a configurar LookAndFeel");
            System.err.println(ex.getLocalizedMessage());

        }

        FrmPrincipal frm = new FrmPrincipal();
        frm.setLocationRelativeTo(null);
        frm.setVisible(true);
    }

}
