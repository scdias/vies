/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package functions;

import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.ws.Holder;

/**
 * Objeto para gerir dados e comunicação com o webservices
 *
 * @author Sergio Dias (scdias@outlook.com)
 */
public class Info {

    private String countryCode;
    private String vatNumber;
    private XMLGregorianCalendar requestDate;
    private Boolean valid;
    private String name;
    private String address;

    /**
     * Construtor genérico
     */
    public Info() {
    }

    /**
     * Retorna o código do país
     *
     * @return código do país
     */
    public String getCountryCode() {
        return countryCode;
    }

    /**
     * Retorna o número fiscal
     *
     * @return Número fiscal
     */
    public String getVatNumber() {
        return vatNumber;
    }

    /**
     * Retorna data do pedido de informação
     *
     * @return Data do pedido de informação
     */
    public XMLGregorianCalendar getRequestDate() {
        return requestDate;
    }

    /**
     * Informa se o número fiscal é válido para efeitos de IVA
     *
     * @return (true/false)(válido/inválido)
     */
    public Boolean getValid() {
        return valid;
    }

    /**
     * Retorna o nome atribuído ao número fiscal
     *
     * @return Nome Fiscal
     */
    public String getName() {
        return name;
    }

    /**
     * Retorna a morada fiscal
     *
     * @return Morada fiscal
     */
    public String getAddress() {
        return address;
    }

    /**
     * Define o código do país para a procura
     *
     * @param countryCode Código do pais em ISO 3166 Alpha-2
     * (https://www.iso.org/obp/ui/#search)
     */
    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    /**
     * Define o número fiscal a ser procurado
     *
     * @param vatNumber Número Fiscal
     */
    public void setVatNumber(String vatNumber) {
        this.vatNumber = vatNumber;
    }

    /**
     * Função responsável a comunicação do webservices
     */
    public void LerInfo() {
        javax.xml.ws.Holder<java.lang.String> _countryCode = new Holder<>(this.countryCode);
        javax.xml.ws.Holder<java.lang.String> _vatNumber = new Holder<>(this.vatNumber);
        javax.xml.ws.Holder<javax.xml.datatype.XMLGregorianCalendar> _requestDate = new Holder<>();
        javax.xml.ws.Holder<Boolean> _valid = new Holder<>();
        javax.xml.ws.Holder<java.lang.String> _name = new Holder<>();
        javax.xml.ws.Holder<java.lang.String> _address = new Holder<>();

        checkVat(_countryCode, _vatNumber, _requestDate, _valid, _name, _address);

        this.countryCode = _countryCode.value;
        this.vatNumber = _vatNumber.value;
        this.requestDate = _requestDate.value;
        this.valid = _valid.value;
        this.name = _name.value;
        this.address = _address.value;

    }

    private static void checkVat(javax.xml.ws.Holder<java.lang.String> countryCode, javax.xml.ws.Holder<java.lang.String> vatNumber, javax.xml.ws.Holder<javax.xml.datatype.XMLGregorianCalendar> requestDate, javax.xml.ws.Holder<Boolean> valid, javax.xml.ws.Holder<java.lang.String> name, javax.xml.ws.Holder<java.lang.String> address) {
        eu.europa.ec.taxud.vies.services.checkvat.CheckVatService service = new eu.europa.ec.taxud.vies.services.checkvat.CheckVatService();
        eu.europa.ec.taxud.vies.services.checkvat.CheckVatPortType port = service.getCheckVatPort();
        port.checkVat(countryCode, vatNumber, requestDate, valid, name, address);
    }

}
