/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package functions;

import java.util.ArrayList;
import java.util.List;

/**
 * Objeto com paises para ser usado na combobox da interface gráfica
 * @author Sergio Dias (scdias@outlook.com)
 */
public class Paises {
    private final List<String> Lista = new ArrayList<>();
    
    /**
     * Construtor genérico
     */
    public Paises(){
        Lista.add("AT-Austria");
        Lista.add("BE-Belgium");
        Lista.add("BG-Bulgaria");
        Lista.add("CY-Chipre");
        Lista.add("CZ-República Checa");
        Lista.add("DE-Alemanha");
        Lista.add("DK-Dinamarca");
        Lista.add("EE-Estónia");
        Lista.add("EL-Grécia");
        Lista.add("ES-Espanha");
        Lista.add("FI-Finlândia");
        Lista.add("FR-França");
        Lista.add("GB-Reino Unido");
        Lista.add("HR-Croácia");
        Lista.add("HU-Hungria");
        Lista.add("IE-Irlanda");
        Lista.add("IT-Itália");
        Lista.add("LT-Lituânia");
        Lista.add("LU-Luxemburgo");
        Lista.add("LV-Letónia");
        Lista.add("MT-Malta");
        Lista.add("NL-Países Baixos");
        Lista.add("PL-Polónia");
        Lista.add("PT-Portugal");
        Lista.add("RO-Roménia");
        Lista.add("SE-Suécia");
        Lista.add("SI-Eslovénia");
        Lista.add("SK-República Eslovaca");        
    }
    
    /**
     * Função para exportar lista de países
     * @return Lista de países
     */
    public List<String> ToList(){
        return Lista;
    }
            
            
}
